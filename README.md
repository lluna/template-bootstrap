Correr una instancia de docker en local

docker run -it --rm --entrypoint sh -v ${PWD}:/data -w /data hashicorp/terraform:0.11.13

Configurar las credenciales

Exportar la variable con el nombre del alumno

export TF_VAR_alumno_slug=nombre.alumno
terraform init
terraform apply

Comitear los cambios al repo

git add .
git commit -m "Creado bucket s3 para guardar estado"
git push origin master
