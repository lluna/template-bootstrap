provider "aws" {}

variable "alumno_slug" {}

resource "aws_s3_bucket" "tfstate" {
  bucket = "devops-bootcamp-202012-${var.alumno_slug}"
  acl    = "private"
}
